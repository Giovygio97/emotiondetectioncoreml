# Emotion Detection with Vision and CoreML
The original model for this project is taken from [this repo](https://github.com/ageitgey/face_recognition).

Then there is also a CoreML Model created with CreateML App, that is more reliable than the original model.v6 model (obtained with the conversion explained in 
"Model conversion with Python"). For the dataset, refer to this github project: https://github.com/muxspace/facial_expressions.git 

The application is realized starting from a simple imageview and a label to demonstrate the recognition and the confidence. It's based on Swift 5.2 and it runs in every iPhone compatible with iOS 13.4 and higher.
The model gives 92% of precision of every of the 6 emotion recognized: anger, contempting, disgusted, fear, happiness, neutral, sadness, surprise.

## Model conversion with python
As Apple reccommends, use coremltools in a virtualenv.

1. First of all, we are gonna install virtualenv: `pip3 install virtualenv`
2. Let's move to the models directory: `cd emotiondetectioncoreml/models`
3. Now we can create our virtualenv: `virtualenv -p /usr/bin/python venv`
4. Then, we activate this env: `source venv/bin/activate`
5. We install the tools required: `pip install coremltools keras==2.2.4 tensorflow==1.14.0`
6. Last but not least, we execute the script which will convert our model: `python script.py`

Finally, you can use the converted model in your XCode project.

For full references on coremltools, please refer to [its repo](https://github.com/apple/coremltools)